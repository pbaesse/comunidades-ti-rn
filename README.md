Comunidades Potiguares de TI
=============

As comunidades potiguares de tecnologia e afins estão bombando na vida real e no Telegram! Participe você também. Esse repositório lista as comunidades (e seus meios de comunicação) por temas na área de tecnologia. Use também para divulgar a comunidade potiguar de TI fora do Telegram. Siga nosso canal no Telegram: [Comunidades Potiguares de TI (listas)](https://t.me/tipotiguar)! Avante!

Inspiração no [Repositório de Canais e Grupos Brasileiros de TI ](https://github.com/alexmoreno/telegram-br). Conheça várias outras comunidades lá também.

## Sempre em construção. Favorite e contribua aqui no repositório com sua edição!

Para realizar pedidos de novas comunidades:

1. Crie um pull request para adicionar novas comunidades;
2. Retire o :triangular_flag_on_post: das comunidades com esse ícone e coloque nas novas comunidades adiciondas;
3. Avise no grupo do Telegram do [PotiLivre](http://t.me/potilivre) ou do [BrejaTec](http://t.me/brejatec) sobre a alteração para que seja atualizado também no canal do Telegram.
   **OBS**: Caso não saiba usar o Git, basta pedir para que sua comunidade seja adicionada aqui no grupo do telegram do [BrejaTec](http://t.me/brejatec) ou do [PotiLivre](http://t.me/potilivre). 

# Canais

- [Agenda TI Potiguar](http://t.me/agendatirn): Nossa agenda de eventos de TI
  - [Instagram](https://www.instagram.com/agendatipotiguar/)
- [Vagas TI RN](http://t.me/VagasTIRN): Consiga sua nova vaga de TI agora

# Temas gerais de TI e socialização

- [Comunidades Potiguares de TI (listas)](https://t.me/tipotiguar): Siga o Canal das Comunidades Potiguares de TI no Telegram
- [BrejaTec](http://t.me/brejatec): BrejaTec - Melhor happy hour mensal de tecnologia  🍻
  - [Twitter](https://twitter.com/brejatec)
  - [Instagram](https://www.instagram.com/brejatec/)
  - [Se inscreva na agenda do Google](http://bit.ly/BrejaTecCalendario)
  - [Facebook](https://www.facebook.com/brejatec/)
- ~~[TI-RN](http://t.me/TIdoRN): Grupo geral de TI~~ 
- [Ágil RN](https://t.me/agilidadeRN): Entusiastas do Filosofia Ágil
- [Jerimum Hackerspace](https://t.me/+QOVJwVj7ctY-LUHR)

# Software livre

- [PotiLivre](http://t.me/potilivre): Comunidade Potiguar de Software Livre
  - [Site](https://potilivre.org/)
  - [Twitter](http://www.twitter.com/potilivre)
  - [Instagram](https://www.instagram.com/potilivre_/)
  - [Facebook](https://www.facebook.com/PotiLivre)

# Linguagens de Programação

- [Grupy RN](http://t.me/grupyrn): Grupo de Usuários de Python
  - [Site](https://meetup.grupyrn.org/)
  - [Facebook](https://www.facebook.com/grupyrn/)
  - [Instagram](https://www.instagram.com/grupyrn/)
  - [GitHub](https://github.com/GruPyRN/)
- [Ruby-RN](http://t.me/ruby_rn): Grupo de Usuários de Ruby
- [PHP-RN](http://t.me/phprn): Grupo de Usuários de PHP
  - [GitHub](https://github.com/phprn)
  - [Site](http://www.phprn.org/)
  - [Twitter](https://twitter.com/php_rn)
  - [Facebook](https://www.facebook.com/groups/1686832664951730)
- [Natal JS](http://t.me/natal_js): Grupo de Usuários de JavaScript
  - [Site](http://nataljs.github.io/)
  - [GitHub](https://github.com/NatalJS)
  - [Facebook](https://www.facebook.com/nataljs/)
- [RustPoti](http://t.me/rustpoti): Linguagem de baixo nível de alto desempenho
- [ElugRN](http://t.me/ElugRN): Grupo de Usuários Elug 
- ~~[Java\<RN\>](https://t.me/java_rn): Java e suas diversas tecnologias~~ 
  - ~~[GitHub](http://github.com/java-rn)~~

# Desenvolvimento aplicado

- Pong: Desenvolvimento de jogos
  - [Discord](https://discordapp.com/invite/Nj44AKA)
  - [Facebook](https://www.facebook.com/groups/pongrn/)
- [Dojo Natal](http://t.me/dojonatal): Treino de programação
- [Blockchain RN](http://t.me/blockchainrn)
- [WebDevRN](http://t.me/webdevrn): Desenvolvimento Web Geral
  - [GitHub](https://github.com/WebDevRN)    
- ~~[MobileDev RN](http://t.me/MobileDevRN): Desenvolvimento Mobile~~
- [WordPress RN](http://t.me/wordpressrn): Comunidade de Usuários do Wordpress
- [Owasp Chapter Natal](https://t.me/owaspnatal): Segurança da informação no desenvolvimento 
- [React Natal](https://t.me/joinchat/H1k6RhYZPseq9vZ1W8A5Eg): Comunidade ReactJS 
  - [Site](https://react.natal.br/)


# Segurança, redes e infraestrutura

- [DevOps RN](http://t.me/devopsrn): Cultura DevOps
- [DarkWaves](http://t.me/darkwaves_group): Redes sem fio
- [Nuvens RN](http://t.me/nuvensrn): Fornecedores de computação em nuvem
- [Infra RN](https://t.me/+Pz0RoEK0JX7Mkj-L): Infraestrutura

# Ciência de Dados

- [Dados Abertos RN](http://t.me/dadosabertosrn): Discussão sobre dados abertos no RN
- [Ciência de dados RN](http://t.me/cienciadedadosRN): Conversa sobre ciência de dados no RN

# Empreendedorismo e empregabilidade

- [Jerimum Valley](http://t.me/jerimumvalley): Empreendedorismo Potiguar
  - ~~[Site](http://jerimumvalley.org/)~~
  - [Facebook](https://www.facebook.com/jerimumvalley/)
  - [Instagram](https://www.instagram.com/jerimumvalley)
- ~~[Divulga Serviços TI - RN](http://t.me/RN_DivulgServsTI): Divulgação de serviços e portfólios de TI~~
- ~~[Remote Natal](http://t.me/remotenatal): Trabalho e vagas de trabalho remoto~~

# Design e comunicação

- [UI/UX UnicoRN](http://t.me/uiuxrn): UI e UX Design
- ~~[Arara Furtacor](https://t.me/afurtacor): Inovação na área de comunicação e design~~
  - ~~[Site](http://afurtacor.online/)~~

# Promoção e reforço da participação feminina na tecnologia
- Mulheres, para entrar nesses grupos do telegram, basta pedir em um grupo como [PotiLivre](http://t.me/potilivre) ou [Grupy RN](http://t.me/grupyrn) que alguma das participantes  adicionarão vocês.
- PyLadies Natal  
  - [Site](https://pyladiesnatal.github.io/)
  - [GitHub](https://github.com/PyLadiesNatal)
  - [Facebook](https://pt-br.facebook.com/PyLadiesNatal/)
- Women Techmakers Natal (GDG)
  - [Facebook](https://www.facebook.com/WTMNatal/)
- R-Ladies Natal
  - [Facebook](https://www.facebook.com/RLadiesNatal/)
  - [Twitter](https://twitter.com/rladiesnatal)
  - [Meetup.com](https://www.meetup.com/pt-BR/rladies-natal/)

# Capítulos locais de organizações internacionais

- GDG - Google Developers Group
  - ~~[Site](http://gdg.natal.br/)~~
  - [Telegram](https://t.me/GDGNatal) 
  - [WhatsApp](https://chat.whatsapp.com/5E6eeDXqEVs38rjqw92mrf)
  - [GitHub](https://github.com/gdg-natal)
  - [Facebook](https://www.facebook.com/gdgnatal/)  
  - ~~[Meetup.com](https://www.meetup.com/pt-BR/GDG-Natal/)~~
- IEEE - Ramo estudantil da IEEE na UFRN
  - [Facebook](https://www.facebook.com/sb.ufrn/)
  - [Instagram](https://www.instagram.com/ieeeufrn/)
  - [LinkedIn](https://pt.linkedin.com/company/sb-ufrn)
- IEEE Computer Society - Capítulo estudantil da IEEE na UFRN com ênfase de trabalho em computação
  - [Site](http://sites.ieee.org/sb-ufrncs/)
  - [Facebook](https://www.facebook.com/csufrn/)
